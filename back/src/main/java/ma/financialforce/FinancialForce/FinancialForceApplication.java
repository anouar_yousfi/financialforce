package ma.financialforce.FinancialForce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class FinancialForceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinancialForceApplication.class, args);
	}

}
