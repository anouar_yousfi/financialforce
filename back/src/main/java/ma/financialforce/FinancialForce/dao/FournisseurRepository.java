package ma.financialforce.FinancialForce.dao;

import ma.financialforce.FinancialForce.entity.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FournisseurRepository extends JpaRepository<Fournisseur, Integer> {
}
