package ma.financialforce.FinancialForce.dao;

import ma.financialforce.FinancialForce.entity.BonDeCommande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "bons-de-commande")
public interface BonDeCommandeRepository extends JpaRepository<BonDeCommande, Integer> {
}
