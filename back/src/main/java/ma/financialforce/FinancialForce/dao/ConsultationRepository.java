package ma.financialforce.FinancialForce.dao;

import ma.financialforce.FinancialForce.entity.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationRepository extends JpaRepository<Consultation, Integer> {
    // todo provide implementation for getConsultationsByUsername
    // public List<Consultation> getConsultationsByUsername(String username);
}
