package ma.financialforce.FinancialForce.dao;

import ma.financialforce.FinancialForce.entity.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProduitRepository extends JpaRepository<Produit, Integer> {
}
