package ma.financialforce.FinancialForce.dao;

import ma.financialforce.FinancialForce.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
