package ma.financialforce.FinancialForce.service;

import ma.financialforce.FinancialForce.dao.BonDeCommandeRepository;
import ma.financialforce.FinancialForce.entity.BonDeCommande;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BonDeCommandeServiceImpl implements BonDeCommandeService {
    private BonDeCommandeRepository bonDeCommandeRepository;

    @Autowired
    public BonDeCommandeServiceImpl(BonDeCommandeRepository produitRepository) {
        this.bonDeCommandeRepository = produitRepository;
    }

    public List<BonDeCommande> findAll() {
        return bonDeCommandeRepository.findAll();
    }
}
