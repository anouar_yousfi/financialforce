package ma.financialforce.FinancialForce.service;

import ma.financialforce.FinancialForce.entity.Consultation;

import java.util.List;

public interface ConsultationService {
    public List<Consultation> findAll();
}
