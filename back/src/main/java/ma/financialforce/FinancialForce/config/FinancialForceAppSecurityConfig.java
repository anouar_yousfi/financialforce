package ma.financialforce.FinancialForce.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class FinancialForceAppSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource securityDataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.jdbcAuthentication() // authentification faite à l'aide des tables users et authoroties dans la bd
            .dataSource(securityDataSource)
            .passwordEncoder(new BCryptPasswordEncoder()); // utilisation de le cryptage BCrypt
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().disable().httpBasic().and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/consultations")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.GET, "/api/consultations/**")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.POST, "/api/consultations")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION")
                .antMatchers(HttpMethod.POST, "/api/consultations/**")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION")
                .antMatchers(HttpMethod.PUT, "/api/consultations")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION")
                .antMatchers(HttpMethod.PUT, "/api/consultations/**")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION")
                .antMatchers(HttpMethod.DELETE, "/api/consultations/**")
                    .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION")

                .antMatchers(HttpMethod.GET, "/api/produits")
                .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION", "CHARGE_ACHATS", "CHEF_PROJET")
                .antMatchers(HttpMethod.GET, "/api/produits/**")
                .hasAnyRole("ADMIN", "CHARGE_COMMUNICATION", "CHARGE_ACHATS", "CHEF_PROJET")
                .antMatchers(HttpMethod.POST, "/api/produits")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS", "CHEF_PROJET")
                .antMatchers(HttpMethod.POST, "/api/produits/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS", "CHEF_PROJET")
                .antMatchers(HttpMethod.PUT, "/api/produits")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS", "CHEF_PROJET")
                .antMatchers(HttpMethod.PUT, "/api/produits/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS", "CHEF_PROJET")
                .antMatchers(HttpMethod.DELETE, "/api/produits/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS", "CHEF_PROJET")

                .antMatchers(HttpMethod.GET, "/api/bons-de-commande")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.GET, "/api/bons-de-commande/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.POST, "/api/bons-de-commande")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.POST, "/api/bons-de-commande/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.PUT, "/api/bons-de-commande")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.PUT, "/api/bons-de-commande/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")
                .antMatchers(HttpMethod.DELETE, "/api/bons-de-commande/**")
                .hasAnyRole("ADMIN", "CHARGE_ACHATS")

                .antMatchers("/api/fournisseurs/**").hasAnyRole("ADMIN", "CHARGE_COMMUNICATION")

                .anyRequest().authenticated() //toutes les requêtes doivent être s'authentifiées avant d'accéder à une ressource
                .and()
                .formLogin()
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }
}