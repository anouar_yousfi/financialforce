package ma.financialforce.FinancialForce.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "fournisseurs")
public class Fournisseur {

    @Id
    @Column(name = "id_fourniss")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idFourniss;

    @Column(name = "nom")
    private String nom;

    @Column(name = "date")
    private Date date;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "rib")
    private String rib;

    @Column(name = "ville")
    private String ville;

    @Column(name = "cnss")
    private String cnss;

    @OneToMany(mappedBy = "fournisseur", cascade = {CascadeType.ALL})
    private List<BonDeCommande> bonsDeCommande;

    @ManyToMany
    @JsonIgnoreProperties("produits")
    @JoinTable(
            name = "fournisseurs_consultations",
            joinColumns = @JoinColumn(name = "id_fourniss"),
            inverseJoinColumns = @JoinColumn(name = "id_consult")
    )
    private List<Consultation> consultations;

    public void add(Consultation consultation) {
        if(consultations == null)
            consultations = new ArrayList<>();

        consultations.add(consultation);
    }

    public void add(BonDeCommande bonDeCommande) {
        if (bonsDeCommande == null) {
            bonsDeCommande = new ArrayList<>();
        }

        bonsDeCommande.add(bonDeCommande);
    }

    public void setBonsDeCommande(List<BonDeCommande> bonsDeCommande) {
        this.bonsDeCommande = bonsDeCommande;
    }

    public List<Consultation> getConsultations() {
        return consultations;
    }

    public int getIdFourniss() {
        return idFourniss;
    }

    public void setIdFourniss(int idFourniss) {
        this.idFourniss = idFourniss;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }
}