package ma.financialforce.FinancialForce.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(name = "username")
    @NotNull
    private String username; // username is constructed from fullname e.g: Soufiane roui => soufiane_roui

    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private int enabled;

    @OneToMany(mappedBy = "user",
                cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JsonIgnoreProperties("produits")
    private List<Consultation> consultations;

    public void add(Consultation consultation) {

        if(consultations == null) {
            consultations = new ArrayList<>();
        }

        consultations.add(consultation);

        consultation.setUser(this);
    }

    public List<Consultation> getConsultations() {
        return consultations;
    }

    public void setConsultations(List<Consultation> consultations) {
        this.consultations = consultations;
    }
}
