package ma.financialforce.FinancialForce.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bon_de_commande")
public class BonDeCommande {

    @Id
    @Column(name = "id_bon_comm")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idConsult;

    @Column(name = "objet")
    private String objet;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "rib")
    private String rib;

    @Column(name = "ville")
    private String ville;

    @Column(name = "cnss")
    private String cnss;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "id_fourniss")
    private Fournisseur fournisseur;

    @ManyToMany
    @JsonIgnoreProperties("bonsDeCommande")
    @JoinTable(
        name = "bon_de_commande_produits",
        joinColumns = @JoinColumn(name = "id_bon_comm"),
        inverseJoinColumns = @JoinColumn(name = "id_produit")
    )
    private List<Produit> produits;

    public void add(Produit produit) {
        if (produits == null)
            produits = new ArrayList<>();

        produits.add(produit);
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    public int getIdConsult() {
        return idConsult;
    }

    public void setIdConsult(int idConsult) {
        this.idConsult = idConsult;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }
}
