package ma.financialforce.FinancialForce.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "produits")
public class Produit {

    @Id
    @Column(name = "id_produit")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idProduit;

    @Column(name = "id_bon_comm")
    private int idBonComm;

    @Column(name = "designation")
    private String designation;

    @Column(name = "categorie")
    private String categorie;

    @Column(name = "qte_commandee")
    private int qteCommandee;

    @ManyToMany
    @JsonIgnoreProperties("produits")
    @JoinTable(
            name = "bon_de_commande_produits",
            joinColumns = @JoinColumn(name = "id_produit"),
            inverseJoinColumns = @JoinColumn(name = "id_bon_comm")
    )
    private List<BonDeCommande> bonsDeCommande;

    @ManyToMany
    @JsonIgnoreProperties("produits")
    @JoinTable(
            name = "produits_consultations",
            joinColumns = @JoinColumn(name = "id_produit"),
            inverseJoinColumns = @JoinColumn(name = "id_consult")
    )
    private List<Consultation> consultations;

    public void add(BonDeCommande bonDeCommande) {
        if (bonsDeCommande == null)
            bonsDeCommande = new ArrayList<>();

        bonsDeCommande.add(bonDeCommande);
    }

    public void add(Consultation consultation) {
        if(consultations == null) {
            consultations = new ArrayList<>();
        }

        consultations.add(consultation);
    }

    public List<BonDeCommande> getBonsDeCommande() {
        return bonsDeCommande;
    }

    public int getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }

    public int getIdBonComm() {
        return idBonComm;
    }

    public void setIdBonComm(int idBonComm) {
        this.idBonComm = idBonComm;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getQteCommandee() {
        return qteCommandee;
    }

    public void setQteCommandee(int qteCommandee) {
        this.qteCommandee = qteCommandee;
    }
}