package ma.financialforce.FinancialForce.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "consultations")
public class Consultation {

    @Id
    @Column(name = "id_consult")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idConsult;

    @Column(name = "corps_courrier")
    private String corpsCourrier;

    @Column(name = "date")
    private Date date;

    @Column(name = "qte_commandee")
    private int qteCommandee;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JsonIgnoreProperties("consultations")
    @JoinColumn(name = "username")
    private User user;

    @ManyToMany
    @JsonIgnoreProperties("consultations")
    @JoinTable(
            name = "produits_consultations",
            joinColumns = @JoinColumn(name = "id_consult"),
            inverseJoinColumns = @JoinColumn(name = "id_produit")
    )
    private List<Produit> produits;

    @ManyToMany
    @JsonIgnoreProperties("consultations")
    @JoinTable(
            name = "fournisseurs_consultations",
            joinColumns = @JoinColumn(name = "id_consult"),
            inverseJoinColumns = @JoinColumn(name = "id_fourniss")
    )
    private List<Fournisseur> fournisseurs;

    public void add(Fournisseur fournisseur) {
        if (fournisseurs == null)
            fournisseurs = new ArrayList<>();

        fournisseurs.add(fournisseur);
    }

    public void add(Produit produit) {
        if (produits == null)
            produits = new ArrayList<>();

        produits.add(produit);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public int getIdConsult() {
        return idConsult;
    }

    public void setIdConsult(int idConsult) {
        this.idConsult = idConsult;
    }

    public String getCorpsCourrier() {
        return corpsCourrier;
    }

    public void setCorpsCourrier(String corpsCourrier) {
        this.corpsCourrier = corpsCourrier;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQteCommandee() {
        return qteCommandee;
    }

    public void setQteCommandee(int qteCommandee) {
        this.qteCommandee = qteCommandee;
    }

    public User getUser() {
        return user;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }
}
